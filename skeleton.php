<?php

/**
 * Skeleton bootstrap file
 *
 *
 * Plugin Name:       Skeleton
 * Plugin URI:        http://reasondigital.com/skeleton
 * Description:       The beginnings of yet another awesome plugin.
 * Version:           1.0.0
 * Author:            Reason Digital
 * Author URI:        http://reasondigital.com/plugins/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' )) {
    die;
}

// get me those src files
require_once 'vendor/autoload.php';

register_activation_hook( __FILE__, array( 'Skeleton\Activator', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Skeleton\Deactivator', 'deactivate' ) );

$plugin = new \Skeleton\Skeleton( __FILE__ );
$plugin->run();