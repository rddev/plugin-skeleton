# Reason Digital Skeleton Plugin #

Where all the skeletons come from.

Requires php 5.4+.

## Composer ##

Uses composer for autoloading. So just add any dependencies to ```composer.json```.

## Config ##

Add any config settings that you will need across your plugin to the ```config.php``` file. You can then access
them like this:

    Config::get('version');
    Config::get('settings.inside.arrays');

## Logging ##

Comes with [monolog](https://github.com/Seldaek/monolog) included out of the box.

    Log::info('something happened');
    Log::info('something else happened with info', ['id' => 1, 'name' => 'Trevor'])
    
    Log::emergency("it's all on fire")
    
The different log levels are debug, info, notice, warning, error, critical, alert and emergency.