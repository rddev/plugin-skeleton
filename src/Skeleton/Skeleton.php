<?php namespace Skeleton;

    /**
     * The file that defines the core plugin class
     *
     * A class definition that includes attributes and functions used across both the
     * public-facing side of the site and the admin area.
     */
use ReasonDigital\Updater;

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 */
class Skeleton
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @var Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @var string $pluginName The string used to uniquely identify this plugin.
     */
    protected $pluginName;

    /**
     * The current version of the plugin.
     *
     * @var string $version The current version of the plugin.
     */
    protected $version;

    /**
     * The base plugin filename.
     *
     * @var string
     */
    protected $pluginFile;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     */
    public function __construct( $pluginFile )
    {

        $this->pluginName = 'skeleton';
        $this->version    = \Config::get( 'version' );
        $this->pluginFile = $pluginFile;

        $this->loader = new Loader();

        $this->defineAdminHooks();
        $this->definePublicHooks();

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     */
    private function defineAdminHooks()
    {

        $plugin_admin = new FacingAdmin\Init( $this->getPluginName(), $this->getVersion() );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueueStyles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueueScripts' );

        $updater = new Updater( $this->pluginFile );

        $this->loader->add_action( 'pre_set_site_transient_update_plugins', $updater, 'setTransient' );
        $this->loader->add_action( 'plugins_api', $updater, 'setPluginInfo', 10, 3 );

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     */
    private function definePublicHooks()
    {

        $plugin_public = new FacingPublic\Init( $this->getPluginName(), $this->getVersion() );

        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueueStyles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueueScripts' );

    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @return    string    The name of the plugin.
     */
    public function getPluginName()
    {
        return $this->pluginName;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @return    Loader    Orchestrates the hooks of the plugin.
     */
    public function getLoader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @return    string    The version number of the plugin.
     */
    public function getVersion()
    {
        return $this->version;
    }

}
