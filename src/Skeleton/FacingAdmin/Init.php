<?php namespace Skeleton\FacingAdmin;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 */
class Init {

	/**
	 * The ID of this plugin.
	 *
	 * @var      string    $skeleton    The ID of this plugin.
	 */
	private $skeleton;

	/**
	 * The version of this plugin.
	 *
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @param      string    $skeleton       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $skeleton, $version ) {

		$this->skeleton = $skeleton;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 */
	public function enqueueStyles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Skeleton_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Skeleton_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->skeleton, plugin_dir_url( __FILE__ ) . 'css/admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 */
	public function enqueueScripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Skeleton_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Skeleton_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->skeleton, plugin_dir_url( __FILE__ ) . 'js/admin.js', array( 'jquery' ), $this->version, false );

	}

}
