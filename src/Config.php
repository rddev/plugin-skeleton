<?php

class Config {

	public static function get($name){

		$file = dirname(__DIR__) .DIRECTORY_SEPARATOR . 'config.php';
		if (!file_exists($file)){
			throw new \Exception("Config file, config.php, is not setup in root of plugin.");
		}

		$config = require $file;

		$pieces = explode('.', $name);
		foreach ($pieces as $piece) {
			if (!is_array($config) || !array_key_exists($piece, $config)) {
				// error occurred
				throw new \InvalidArgumentException('That config key does not exist.');
			}
			$config = &$config[$piece];
		}

		return $config;
	}
}